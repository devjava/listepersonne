<%--
  Created by IntelliJ IDEA.
  User: Pascal Bouchez
  Date: 05/02/2019
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html lang="fr">
<head>
    <title>Liste personnes</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Adresse mail</th>
                <th>Adresse</th>
                <th>Modifier</th>
                <th>Supprimer</th>
            </tr>
        <thead/>
        <tr>
            <form action="${context}/person" method="POST">
            <td><input name="lastName" type="text" value="${pers.getLastName()}"/></td>
            <td><input name="firstName" type="text" value="${pers.getFirstName()}"/></td>
            <td><input name="email" type="text" value="${pers.getEmail()}"></td>
            <td class="hidden"><input name="id" type="hidden" value="${pers.getId()}"></td>
            <td><input class="btn btn-primary" type="submit"></td>
            </form>
        </tr>
        <c:forEach items="${persons}" var="person" varStatus="loop">
        <tr>
            <td><c:out value="${person.getLastName()}"></c:out></td>
            <td><c:out value="${person.getFirstName()}"></c:out></td>
            <td><c:out value="${person.getEmail()}"></c:out></td>
            <td>
                <ul>
                    <c:forEach items="${person.getAdress()}" var="adress">
                        <li><c:out value="${adress.getRue()}"></c:out> <c:out value="${adress.getCp()}"></c:out></li>
                    </c:forEach>
                </ul>
            </td>
            <td><a href="${context}/person/<c:out value="${person.id}"></c:out>" class="btn btn-outline-success">Modifier</a></td>
            <td><a href="${context}/suprr/<c:out value="${person.id}"></c:out>" class="btn btn-outline-danger">Supprimer</a></td>
        </tr>
        </c:forEach>
    </table>


</body>
</html>
