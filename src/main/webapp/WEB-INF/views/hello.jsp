<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8" />
<title>test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body>
<form action="person" method="POST">
    <table class="table">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Adresse mail</th>
            <th>Valider</th>
        </tr>
        <thead/>
        <tr>
            <td><input name="lastName" type="text"/></td>
            <td><input name="firstName" type="text"/></td>
            <td><input name="email" type="text"></td>
            <td><input class="btn btn-primary" type="submit"></td>
        </tr>
    </table>
    </form>
</body>
</html>
