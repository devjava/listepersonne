package fr.imie.manager;

import java.util.List;

/**
 * Class Manager generic
 *
 * @param <T>
 */
public interface Manager<T> {

    public void add(T object);

    public void deleteById(Integer id);

    public void delete(T object);

    public T findById(Integer id);

    public List<T> getAll();
}
