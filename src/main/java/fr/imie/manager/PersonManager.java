package fr.imie.manager;

import fr.imie.bo.PersonEntity;
import fr.imie.controller.HelloController;
import fr.imie.dao.PersonDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Class PersonManager
 *
 * Implements Manager<PersonEntity>
 *
 */
@Service
public class PersonManager implements Manager<PersonEntity> {

    private final Logger logger = LoggerFactory.getLogger ( HelloController.class );

    @Autowired
    PersonDao personneDao;

    @Override
    public void add(PersonEntity object) {
        logger.info("Add database");
        personneDao.save(object);
    }

    @Override
    public void deleteById(Integer id) {
        logger.info("Delete by id");
        personneDao.delete(findById(id));
    }

    @Override
    public void delete(PersonEntity object) {
        logger.info("Delete object database");
        personneDao.delete(object);
    }

    @Override
    public PersonEntity findById(Integer id) {
        return personneDao.findById(id).get();
    }

    @Override
    public List<PersonEntity> getAll() {
        logger.info("Return all PersonEntity");
        return personneDao.findAll();
    }
}
