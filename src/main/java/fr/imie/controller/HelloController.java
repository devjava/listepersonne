package fr.imie.controller;

import fr.imie.bo.AdressEntity;
import fr.imie.bo.PersonEntity;
import fr.imie.manager.PersonManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller HelloController
 */
@Controller
public class HelloController {

    @Autowired
    PersonManager personManager;

    private final Logger logger = LoggerFactory.getLogger ( HelloController.class );

    /**
     * Page hello.jsp
     *
     * @return hello.jsp
     */
    @RequestMapping("hello")
    public String hello() {
        return "hello";
    }

    /**
     * Add or Update person
     *
     * @param person
     * @param model
     * @return person.jsp
     */
    @PostMapping("person")
    public String addPerson(PersonEntity person, Model model){
        logger.info("Add person");

        List<AdressEntity> adress = new ArrayList<>();
        adress.add(new AdressEntity("rue des dev","44000"));
        adress.add(new AdressEntity("rue des java","44300"));
        adress.add(new AdressEntity("rue des j2ee","44200"));
        adress.add(new AdressEntity("rue des oracle","44000"));

        person.setAdress(adress);

        personManager.add(person);

        model.addAttribute("persons",personManager.getAll());

        return "person";
    }

    /**
     * Delete person
     *
     * @param id
     * @param model
     * @return person.jsp
     */
    @GetMapping("suprr/{id}")
    public String deletePerson(@PathVariable Integer id,Model model){
        logger.info("Delete person");
        personManager.deleteById(id);

        model.addAttribute("persons",personManager.getAll());

        return "person";
    }

    /**
     * Get person value
     * @param id
     * @param model
     * @return person.jsp
     */
    @GetMapping("person/{id}")
    public String getPerson(@PathVariable Integer id,Model model){
        logger.info("Update person");
        model.addAttribute("pers",personManager.findById(id));
        model.addAttribute("persons",personManager.getAll());

        return "person";
    }

}
