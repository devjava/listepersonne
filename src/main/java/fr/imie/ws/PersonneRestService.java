package fr.imie.ws;

import fr.imie.bo.PersonEntity;
import fr.imie.manager.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/persons")
public class PersonneRestService {

    @Autowired
    PersonManager personManager;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/testJersey")
    public String testJersey(){
        return "Hello Jersey";
    }

    @GET
    @Path("{id}")
    public PersonEntity getPerson(@PathParam("id") Integer id){
        return personManager.findById(id);
    }

    @GET
    @Path("/test")
    public List<PersonEntity> getAllPersons(){
        return personManager.getAll();
    }

    @POST
    @Path("/addupdate")
    public List<PersonEntity> addPerson(PersonEntity personEntity){
        personManager.add(personEntity);
        return personManager.getAll();
    }

    @GET
    @Path("/delete/{id}")
    public List<PersonEntity> deletePerson(@PathParam("id") Integer id){
        personManager.deleteById(id);
        return personManager.getAll();
    }






}
