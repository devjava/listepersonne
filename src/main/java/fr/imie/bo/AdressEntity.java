package fr.imie.bo;

import javax.persistence.*;

/**
 * Entity adress
 *
 * id
 * rue
 * cp
 *
 */
@Entity
@Table(name = "adress")
public class AdressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String rue;

    @Column(nullable = false)
    private String cp;

    public AdressEntity() {
    }

    public AdressEntity(String rue, String cp) {
        this.rue = rue;
        this.cp = cp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AdressEntity{");
        sb.append("id=").append(id);
        sb.append(", rue='").append(rue).append('\'');
        sb.append(", cp='").append(cp).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
