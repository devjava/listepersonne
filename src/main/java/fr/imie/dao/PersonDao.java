package fr.imie.dao;

import fr.imie.bo.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface dao PersonDao
 *
 * Extends JpaRepository<PersonEntity,Integer>
 *
 */
@Repository
public interface PersonDao extends JpaRepository<PersonEntity,Integer> {
}
